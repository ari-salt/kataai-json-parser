from flask import Flask, make_response, render_template, request, Response
import csv, json, os

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def home():
	if request.method == 'POST':
		_type = request.form['_type']
		csv_old = request.files['csv']
		csv_old.save('original_' + _type + '.csv')
		csv_new = ''

		with open('original_' + _type + '.csv', mode='r') as csv_file:
			csv_reader = csv.DictReader(csv_file)

			if _type == 'prediction':
				csv_new += '_id,input,trained,created_at,corrected,predicted_kata_types,predicted_kata_scores,predicted_kata_values,predicted_kata_labels,predicted_products_types,predicted_products_scores,predicted_products_values,predicted_products_labels,predicted_intents_scores,predicted_intents_values,predicted_asks_scores,predicted_asks_values,predicted_sentiments_scores,predicted_sentiments_values,predicted_topics_scores,predicted_topics_values\n'

				for row in csv_reader:
					predicted = json.loads(row["predicted"])
					predicted_kata_types = []
					predicted_kata_scores = []
					predicted_kata_values = []
					predicted_kata_labels = []
					predicted_products_types = []
					predicted_products_scores = []
					predicted_products_values = []
					predicted_products_labels = []
					predicted_intents_scores = []
					predicted_intents_values = []
					predicted_asks_scores = []
					predicted_asks_values = []
					predicted_sentiments_scores = []
					predicted_sentiments_values = []
					predicted_topics_scores = []
					predicted_topics_values = []

					for kata in predicted['kata']:
						if 'type' in kata.keys():
							predicted_kata_types.append(kata['type'])
						if 'score' in kata.keys():
							predicted_kata_scores.append(str(kata['score']))
						if 'value' in kata.keys():
							predicted_kata_values.append(str(kata['value']))
						if 'label' in kata.keys():
							predicted_kata_labels.append(str(kata['label']))
					
					for products in predicted['products']:
						if 'type' in products.keys():
							predicted_products_types.append(products['type'])
						if 'score' in products.keys():
							predicted_products_scores.append(str(products['score']))
						if 'value' in products.keys():
							predicted_products_values.append(str(products['value']))
						if 'label' in products.keys():
							predicted_products_labels.append(str(products['label']))

					for intents in predicted['intents']:
						if 'score' in intents.keys():
							predicted_intents_scores.append(str(intents['score']))
						if 'value' in intents.keys():
							predicted_intents_values.append(str(intents['value']))

					for asks in predicted['asks']:
						if 'score' in asks.keys():
							predicted_asks_scores.append(str(asks['score']))
						if 'value' in asks.keys():
							predicted_asks_values.append(str(asks['value']))

					for sentiments in predicted['sentiments']:
						if 'score' in sentiments.keys():
							predicted_sentiments_scores.append(str(sentiments['score']))
						if 'value' in sentiments.keys():
							predicted_sentiments_values.append(str(sentiments['value']))

					for topics in predicted['topics']:
						if 'score' in topics.keys():
							predicted_topics_scores.append(str(topics['score']))
						if 'value' in topics.keys():
							predicted_topics_values.append(str(topics['value']))

					predicted_kata_types = ','.join(predicted_kata_types)
					predicted_kata_scores = ','.join(predicted_kata_scores)
					predicted_kata_values = ','.join(predicted_kata_values)
					predicted_kata_labels = ','.join(predicted_kata_labels)
					predicted_products_types = ','.join(predicted_products_types)
					predicted_products_scores = ','.join(predicted_products_scores)
					predicted_products_values = ','.join(predicted_products_values)
					predicted_products_labels = ','.join(predicted_products_labels)
					predicted_intents_scores = ','.join(predicted_intents_scores)
					predicted_intents_values = ','.join(predicted_intents_values)
					predicted_asks_scores = ','.join(predicted_asks_scores)
					predicted_asks_values = ','.join(predicted_asks_values)
					predicted_sentiments_scores = ','.join(predicted_sentiments_scores)
					predicted_sentiments_values = ','.join(predicted_sentiments_values)
					predicted_topics_scores = ','.join(predicted_topics_scores)
					predicted_topics_values = ','.join(predicted_topics_values)

					csv_new += f'{row["_id"]},"{row["input"]}",{row["trained"]},{row["created_at"]},"{row["corrected"]}","{predicted_kata_types}","{predicted_kata_scores}","{predicted_kata_values}","{predicted_kata_labels}","{predicted_products_types}","{predicted_products_scores}","{predicted_products_values}","{predicted_products_labels}","{predicted_intents_scores}","{predicted_intents_values}","{predicted_asks_scores}","{predicted_asks_values}","{predicted_sentiments_scores}","{predicted_sentiments_values}","{predicted_topics_scores}","{predicted_topics_values}"\n'

			if _type == 'training':
				csv_new += 'id,input,entity_entities,entity_labels,entity_values,created_at,updated_at\n'

				for row in csv_reader:
					entities = json.loads(row["entities"])
					entity_entities = []
					entity_labels = []
					entity_values = []

					for entity in entities:
						if 'entity' in entity.keys():
							entity_entities.append(str(entity['entity']))
						if 'label' in entity.keys():
							entity_labels.append(str(entity['label']))
						if 'value' in entity.keys():
							entity_values.append(str(entity['value']))
					
					entity_entities = ','.join(entity_entities)
					entity_labels = ','.join(entity_labels)
					entity_values = ','.join(entity_values)

					csv_new += f'{row["id"]},"{row["input"]}","{entity_entities}","{entity_labels}","{entity_values},{row["created_at"]},{row["updated_at"]}"\n'

		os.remove('original_' + _type + '.csv')

		return Response(csv_new, mimetype='text/csv', headers={'Content-disposition': 'attachment; filename=' + _type + '.csv'})
	else:
		return render_template('home.html')

if __name__ == '__main__':
	app.run(debug=True)